package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HelloController {

    @GetMapping({"/"})
    public String home() {
        return "login";
    }

    @GetMapping({"/hello"})
    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
    	model.addAttribute("name", name);
    	return "hello";
    }
   
	@ModelAttribute
	public void setResponseHeader(HttpServletResponse response) {
        //response.setHeader("Access-Control-Allow-Origin", "*");
	}
}

